package main

import (
	"fmt"
	"net/http"
	"time"
)

func main() {

	links := []string{
		"http://google.com",
		"http://facebook.com",
		"http://github.com",
		"http://golang.org",
	}

	c := make(chan string)

	for _, link := range links {
		//make
		go healthCheck(link, c)

	}

	//for i := 0; i < len(links); i++ {
	//	fmt.Println(<-c)
	//}

	//	for { // indefinite for loop.
	//		go healthCheck(<-c, c)
	//	}

	//	for l := range c { //indefinite for loop using range on channels (wait until channel gets messages)
	//		go healthCheck(l, c)
	//	}

	for k := range c { //indefinite for loop using range on channels (wait until channel gets messages)
		go func(link string) {
			time.Sleep(3 * time.Second)
			healthCheck(link, c)
		}(k)
	}

	//fmt.Println(<-c)
	//fmt.Println(<-c)
	//fmt.Println(<-c)
	//fmt.Println(<-c)
	//fmt.Println(<-c)

}

func healthCheck(link string, ch chan string) {
	_, err := http.Get(link)
	if err != nil {
		fmt.Printf("It seems %v is down", link)
		ch <- link
		return
	}
	fmt.Printf("%v is up!", link)
	ch <- link
	return

}
