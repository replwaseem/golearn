package main

import "fmt"

type bot interface {
	getGreeting() string
}

type englishbot struct{}

type spanishbot struct{}

func (englishbot) getGreeting() string {
	return "Hi There!"
}

func (spanishbot) getGreeting() string {
	return "Hola"
}
func printGreeting(b bot) {
	fmt.Println(b.getGreeting())
}

func main() {
	engb := englishbot{}
	sb := spanishbot{}

	printGreeting(engb)
	printGreeting(sb)
}
