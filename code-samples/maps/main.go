package main

import "fmt"

func main() {

	// declaration style1
	colors := map[string]string{
		"red":   "#ff0000",
		"green": "#fd9893",
		"blue":  "#204s90",
	}

	// declaration style 2
	var games map[string]string

	// declaration style 3
	cities := make(map[string]string)

	cities["bangalore"] = "karnataka"

	delete(colors, "blue")

	fmt.Println(colors)
	fmt.Println(games)
	fmt.Println(cities)

	printColors(colors)
	printColors(colors)
}

func printColors(m map[string]string) {
	for color, hex := range m {
		fmt.Printf("Color: %v  HexCode: %v", color, hex)
		fmt.Println()
	}

	m["Pink"] = "#44957"
}
