package main

import "testing"

func TestNewDeck(t *testing.T) {
	de := newDeck()

	if len(de) != 9 {
		t.Errorf("Expected deck length of 9, but go %v", len(de))
	}
}
