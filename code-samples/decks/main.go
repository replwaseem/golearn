package main

import "fmt"

func main() {

	cards := newDeck()

	//cards.print()

	deck1, deck2 := deal(cards, 5)

	//deck1.print()

	//deck2.print()

	fmt.Println(deck1.toString())
	fmt.Println(deck2.toString())

	cards.saveToFile("my-cards")

	cards = toDeckFromFile("my-cards")

	cards.shuffle()

	cards.print()
}
