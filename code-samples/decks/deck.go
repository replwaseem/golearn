package main

import (
	"fmt"
	"io/ioutil"
	"math/rand"
	"os"
	"strings"
	"time"
)

type deck []string

func newDeck() deck {
	cards := deck{}

	cardSuits := []string{"Spades", "Hearts", "Diamonds"}
	cardValues := []string{"Ace", "Two", "Three"}

	for _, suit := range cardSuits {
		for _, value := range cardValues {
			cards = append(cards, value+" of "+suit)
		}
	}

	return cards

}

func (d deck) print() {
	for i, card := range d {
		fmt.Println(i, card)
	}
}

func deal(d deck, handsize int) (deck, deck) {
	return d[:handsize], d[handsize:]
}

func (d deck) toString() string {

	full := strings.Join([]string(d), "%")
	return full
}

func (d deck) saveToFile(filename string) error {

	return ioutil.WriteFile(filename, []byte(d.toString()), 0666)

}

func toDeckFromFile(filename string) deck {

	byteSlice, error := ioutil.ReadFile(filename)

	if error != nil {
		fmt.Println("Error reading deck file", error)
		os.Exit(1)
	}
	var cards = strings.Split(string(byteSlice), "%")
	return deck(cards)
}

func (d deck) shuffle() {

	source := rand.NewSource(time.Now().UnixNano())
	random := rand.New(source)
	for i := range d {
		newposition := random.Intn(len(d) - 1)
		d[i], d[newposition] = d[newposition], d[i]
	}
}
