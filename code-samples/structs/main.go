package main

import "fmt"

type person struct {
	firstName string
	lastName  string
	contact   contactinfo
}

type contactinfo struct {
	email   string
	zipcode int
}

func (p person) profile() {
	fmt.Printf("Hello there %v", p)
}

func (p *person) updateFirstName(firstName string) {
	(*p).firstName = firstName
}

func main() {

	var newPerson person
	var contact contactinfo
	contact.email = "waseem.shk@gmail.com"
	contact.zipcode = 560029
	newPerson.firstName = "Waseem"
	newPerson.lastName = "Shaik"
	newPerson.contact = contact
	//pointerToPerson := &newPerson
	//pointerToPerson.updateFirstName("Jimmy")
	newPerson.updateFirstName("Jimmy")
	newPerson.profile()

}
