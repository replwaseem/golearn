package main

import (
	"fmt"
	"io"
	"net/http"
	"os"
)

// interfaces are implicit in go.
type logWriter struct{}

// implementing Writer interface
func (l logWriter) Write(b []byte) (int, error) {
	fmt.Println("No of bytes read", len(b))
	return len(b), nil
}

func main() {

	res, err := http.Get("http://google.com")

	if err != nil {
		fmt.Println("Error: ", err)
		os.Exit(1)
	}
	//bs := make([]byte, 99999)
	lw := logWriter{}
	//res.Body.Read(bs)
	//fmt.Println(string(bs))
	io.Copy(lw, res.Body)
}
